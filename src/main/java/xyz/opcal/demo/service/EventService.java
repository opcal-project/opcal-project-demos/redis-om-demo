package xyz.opcal.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.opcal.demo.domain.Event;
import xyz.opcal.demo.repository.documents.EventRepository;

@Service
public class EventService {

	private @Autowired EventRepository eventRepository;

	public Event save(Event event) {
		return eventRepository.save(event);
	}

	public List<Event> all() {
		return eventRepository.findAll();
	}
}
