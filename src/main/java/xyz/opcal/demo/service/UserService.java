package xyz.opcal.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.opcal.demo.domain.User;
import xyz.opcal.demo.repository.hashes.UserRepository;

@Service
public class UserService {

	private @Autowired UserRepository userRepository;

	public User save(User user) {
		return userRepository.save(user);
	}

	public Iterable<User> all() {
		return userRepository.findAll();
	}
}
