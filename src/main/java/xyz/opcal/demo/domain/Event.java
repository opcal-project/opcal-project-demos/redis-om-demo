package xyz.opcal.demo.domain;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.index.Indexed;

import com.redis.om.spring.annotations.Document;
import com.redis.om.spring.annotations.Searchable;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@Document
public class Event {

	@Id
	private Long id;

	@NonNull
	@Searchable
	private String name;

	@Indexed
	private LocalDateTime beginDate;

	@Indexed
	private LocalDateTime endDate;
}
