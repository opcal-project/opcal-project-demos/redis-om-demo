package xyz.opcal.demo.test;

import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.lifecycle.Startables;

import com.redis.testcontainers.RedisStackContainer;

public abstract class AbstractRedisStackTest {

	static RedisStackContainer redisStackContainer = new RedisStackContainer(RedisStackContainer.DEFAULT_IMAGE_NAME.withTag(RedisStackContainer.DEFAULT_TAG));

	@DynamicPropertySource
	static void updateRedis(DynamicPropertyRegistry registry) {
		Startables.deepStart(redisStackContainer).join();

		registry.add("spring.data.redis.host", redisStackContainer::getHost);
		registry.add("spring.data.redis.port", redisStackContainer::getFirstMappedPort);
	}

}
