package xyz.opcal.demo.service;

import java.time.LocalDateTime;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import xyz.opcal.demo.domain.Event;
import xyz.opcal.demo.test.AbstractRedisStackTest;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EventServiceTest extends AbstractRedisStackTest{

	@Autowired
	EventService eventService;

	@Test
	@Order(0)
	void save() {
		Event event = new Event();
		event.setName("e:" + System.currentTimeMillis());
		event.setBeginDate(LocalDateTime.now());
		event.setEndDate(LocalDateTime.now().plusHours(3));
		System.out.println(eventService.save(event));
	}

	@Test
	@Order(1)
	void all() {
		System.out.println(eventService.all());
	}

}
