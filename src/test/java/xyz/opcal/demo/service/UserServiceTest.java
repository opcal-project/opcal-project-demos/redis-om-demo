package xyz.opcal.demo.service;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import xyz.opcal.demo.domain.User;
import xyz.opcal.demo.test.AbstractRedisStackTest;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserServiceTest extends AbstractRedisStackTest{

	@Autowired
	UserService userService;

	@Test
	@Order(0)
	void save() {
		User user = new User();
		var t = System.currentTimeMillis();
		user.setFirstName("TF" + t);
		user.setLastName("TL" + t);
		user.setEmail("TF" + t + "@t.com");
		System.out.println(userService.save(user));
	}

	@Test
	@Order(1)
	void all() {
		userService.all().forEach(System.out::println);
	}

}
